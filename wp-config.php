<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'est_elagage' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+A@n_Y<zL&|No<s:X]Q7^)7P^QtRR!{O;%@UPB~Oe{bVnD*h!(Wkvm)@QRc0FxH7' );
define( 'SECURE_AUTH_KEY',  '94lO^E31P jc}TM+@SSxJ!(TrEsOzudd&|(yqD5Xy^)zDp)Qb*gN3u+&nB!&N~i7' );
define( 'LOGGED_IN_KEY',    '<%nRa2f*HVyDd~-Y UGpy+eaV>6a<]<Cc)NEau?iv9&^;^^<CBImj#S8D*#}{4sw' );
define( 'NONCE_KEY',        'YRVs-[t1vetiu1<)75Z|anE!GF#Ih($+&-w(8@2Rz;:&zoC!;,xp]PUEcxQ3!%-r' );
define( 'AUTH_SALT',        'Ba5|hH?wp0u5>[5a&=J_[cPqtx^smDHODwU<iv(iwLP}sB=q<;SR86@dh5pE6*r1' );
define( 'SECURE_AUTH_SALT', 'l5#EYEW-=Hg[o9>.$K3o>!@OhBKGxBF,oK^]Y~HYqi_nIu!+NxPP2/%(]1kk6,3j' );
define( 'LOGGED_IN_SALT',   'I.w.Ai;#4jObt$|k2j-OksR7yUp2S@vxU&O_yd#}2Azvf<pK-$qL2Kt-_Uo^NF/Y' );
define( 'NONCE_SALT',       'd-d3gdFkN2WP=<[gN<#BxPFM@5i2(Wr@_=CYX/felf>S8iEm[}yxxI?M7H%1jLih' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

