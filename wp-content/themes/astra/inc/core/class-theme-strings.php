<?php
/**
 * Astra Theme Strings
 *
 * @package     Astra
 * @author      Astra
 * @copyright   Copyright (c) 2020, Astra
 * @link        https://wpastra.com/
 * @since       Astra 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Default Strings
 */
if ( ! function_exists( 'astra_default_strings' ) ) {

	/**
	 * Default Strings
	 *
	 * @since 1.0.0
	 * @param  string  $key  String key.
	 * @param  boolean $echo Print string.
	 * @return mixed        Return string or nothing.
	 */
	function astra_default_strings( $key, $echo = true ) {

		$defaults = apply_filters(
			'astra_default_strings',
			array(

				// Header.
				'string-header-skip-link'                => __( 'Skip to content', 'astra' ),

				// 404 Page Strings.
				'string-404-sub-title'                   => __( 'It looks like the link pointing here was faulty. Maybe try searching?', 'astra' ),

				// Search Page Strings.
				'string-search-nothing-found'            => __( 'Nous n\'avons rien trouvé', 'astra' ),
				'string-search-nothing-found-message'    => __( 'Désolé, mais rien ne correspond à vos termes de recherche. Veuillez réessayer avec d\'autres mots-clés.', 'astra' ),
				'string-full-width-search-message'       => __( 'Commencez à taper et appuyez sur Entrée pour rechercher', 'astra' ),
				'string-full-width-search-placeholder'   => __( 'Recherche &hellip;', 'astra' ),
				'string-header-cover-search-placeholder' => __( 'Recherche &hellip;', 'astra' ),
				'string-search-input-placeholder'        => __( 'Recherche &hellip;', 'astra' ),

				// Comment Template Strings.
				'string-comment-reply-link'              => __( 'Répondre', 'astra' ),
				'string-comment-edit-link'               => __( 'Editer', 'astra' ),
				'string-comment-awaiting-moderation'     => __( 'Votre commentaire est en attente de modération.', 'astra' ),
				'string-comment-title-reply'             => __( 'Laisser un commentaire', 'astra' ),
				'string-comment-cancel-reply-link'       => __( 'Annuler réponse', 'astra' ),
				'string-comment-label-submit'            => __( 'Poster le commentaire &raquo;', 'astra' ),
				'string-comment-label-message'           => __( 'Taper ici..', 'astra' ),
				'string-comment-label-name'              => __( 'Nom*', 'astra' ),
				'string-comment-label-email'             => __( 'Email*', 'astra' ),
				'string-comment-label-website'           => __( 'Site Web', 'astra' ),
				'string-comment-closed'                  => __( 'Les commentaires sont fermés.', 'astra' ),
				'string-comment-navigation-title'        => __( 'Comment navigation', 'astra' ),
				'string-comment-navigation-next'         => __( 'Commentaires récents', 'astra' ),
				'string-comment-navigation-previous'     => __( 'Commentaires plus anciens', 'astra' ),

				// Blog Default Strings.
				'string-blog-page-links-before'          => __( 'Pages:', 'astra' ),
				'string-blog-meta-author-by'             => __( 'Par ', 'astra' ),
				'string-blog-meta-leave-a-comment'       => __( 'Laisser un commentaire', 'astra' ),
				'string-blog-meta-one-comment'           => __( '1 commentaire', 'astra' ),
				'string-blog-meta-multiple-comment'      => __( '% commentaires', 'astra' ),
				'string-blog-navigation-next'            => __( 'Page suivante', 'astra' ) . ' <span class="ast-right-arrow">&rarr;</span>',
				'string-blog-navigation-previous'        => '<span class="ast-left-arrow">&larr;</span> ' . __( 'Page précédente', 'astra' ),

				// Single Post Default Strings.
				'string-single-page-links-before'        => __( 'Pages:', 'astra' ),
				/* translators: 1: Post type label */
				'string-single-navigation-next'          => __( '%s suivant', 'astra' ) . ' <span class="ast-right-arrow">&rarr;</span>',
				/* translators: 1: Post type label */
				'string-single-navigation-previous'      => '<span class="ast-left-arrow">&larr;</span> ' . __( '%s précédent', 'astra' ),

				// Content None.
				'string-content-nothing-found-message'   => __( 'Nous ne pouvons pas trouver ce que vous cherchez. Peut-être que la recherche peut aider.', 'astra' ),

			)
		);

		if ( is_rtl() ) {
			$defaults['string-blog-navigation-next']     = __( 'Page suivante', 'astra' ) . ' <span class="ast-left-arrow">&larr;</span>';
			$defaults['string-blog-navigation-previous'] = '<span class="ast-right-arrow">&rarr;</span> ' . __( 'Page précédente', 'astra' );

			/* translators: 1: Post type label */
			$defaults['string-single-navigation-next'] = __( '%s suivant', 'astra' ) . ' <span class="ast-left-arrow">&larr;</span>';
			/* translators: 1: Post type label */
			$defaults['string-single-navigation-previous'] = '<span class="ast-right-arrow">&rarr;</span> ' . __( '%s précédent', 'astra' );
		}

		$output = isset( $defaults[ $key ] ) ? $defaults[ $key ] : '';

		/**
		 * Print or return
		 */
		if ( $echo ) {
			echo $output; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		} else {
			return $output;
		}
	}
}
